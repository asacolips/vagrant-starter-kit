# Setup Instructions

### Clone the repository

```
git clone git@bitbucket.org:asacolips/vagrant-starter-kit.git
```

### Add the vagrant box to your hostfile

```
192.168.34.17 vagrant-box-name.dev
```

### Setup vagrant

```
cd ~/Sites/vagrant-starter-kit/vagrant_box_name
vagrant up
```

### Import the database

```
vagrant ssh
mysql -uroot -proot
CREATE DATABASE database_name;
exit

If you already have a database and need to import it, then you should also run the following command before using `exit`. Store the copy of your file.sql file in the `/mysql` directory of this repository.
```
mysql -uroot -proot database_name < file.sql
```

### Leave the vagrant session

```
exit
```

### If you haven't already done so add the vagrant key to your .ssh:

```
ln -s ~/.vagrant.d/insecure_private_key ~/.ssh/vagrant_insecure_private_key
chmod 600 ~/.ssh/vagrant_insecure_private_key
```

### Add vagrant-box-name.dev to your ~/.ssh/config

```
Host vagrant-box-name.dev
  ForwardAgent yes
  IdentityFile ~/.ssh/vagrant_insecure_private_key
  User vagrant
```

### Clear caches

```
cd ~/Sites/vagrant-box-name/docroot
drush @vagrant-box-name.dev cc all
```

# Theming Setup

Gulp is used to compile Sass and compress images. It is located outside the docroot to prevent Drupal from slowing down during its directory scan. The following section assumes you're in the `gulp` directory of the repo. Gulp uses the `themename` theme as its destination directory for compiled code and minifed images.

### Setup gulp

First, install node locally using nvm

```
cd gulp
nvm install node
```

Second, run npm install. Because this is a local install, you may have to run `source ~/.bash_profile` after a reboot in order to get npm recognized.

```
npm install
```

### Run gulp (compile Sass)

```
npm run gulp
```

### Run gulp in watch mode for file changes

```
npm run gulp watch
```

### Run gulp in compress mode for production

```
npm run gulp compress
```