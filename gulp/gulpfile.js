'use strict';

//=======================================================
// Include gulp
//=======================================================
var gulp = require('gulp');

//=======================================================
// Include Our Plugins
//=======================================================
var sass       = require('gulp-sass');
var prefix     = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
//var sync       = require('browser-sync');
//var reload     = sync.reload;
var filter     = require('gulp-filter');
var imagemin   = require('gulp-imagemin');
var pngquant   = require('imagemin-pngquant');

//=======================================================
// Variables
//=======================================================
var src_path = '../docroot/sites/all/themes/custom/themename/';

//=======================================================
// Functions
//=======================================================
function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}

//=======================================================
// Compile Our Sass
//=======================================================
gulp.task('sass', function() {
  gulp.src(src_path + 'sass/{,**/}*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'nested'
    }))
    .on('error', handleError)
    .pipe(prefix({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest(src_path + 'css'))
    .pipe(filter('*.css'));
    /*
    .pipe(sync.reload({
      stream: true
    }));
    */
});

//=======================================================
// Watch and recompile sass.
//=======================================================
gulp.task('watch', function() {

  // BrowserSync proxy setup
  //sync({
      //proxy: 'http://nameoflocalhere.mcdev'
  //});

  // Watch all my sass files and compile sass if a file changes.
  gulp.watch(src_path + 'sass/{,**/}*.scss', ['sass']);

});

//=======================================================
// Compress assets (images, pngs, svgs).
//=======================================================
gulp.task('compress', function () {
  return gulp.src([
      src_path + 'assets/src/icons/*',
      src_path + 'assets/src/images/*'
    ], { base: src_path + 'assets/src/' })
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{
        removeViewBox: false
      }],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(src_path + 'assets/dist'));
});

//=======================================================
// Default Task
//=======================================================
gulp.task('default', ['sass', 'compress']);
